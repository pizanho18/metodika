<?php

namespace MetodikaTI\Http\Controllers\Back\Dashboard\System;

use MetodikaTI\Http\Requests\Back\System\Podcast\CreateRequest;
use MetodikaTI\Http\Requests\Back\System\Podcast\EditRequest;
use MetodikaTI\Http\Controllers\Controller;
use MetodikaTI\Library\URI;
use MetodikaTI\Podcast;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class PodcastController extends Controller
{
    public function index()
    {
        //$data = DB::table('podcast')->get();
        $data = Podcast::orderBy('id')->get();
        return view('back.dashboard.system.podcast.index',  array('data'=>$data, 'permitions' => URI::checkPermitions()));
    }

    public function create(){
        return view('back.dashboard.system.podcast.create',  array('permitions' => URI::checkPermitions() ) );
    }

    public function store(CreateRequest $request){

        $response = array('status' => false);

        $podcast    = new Podcast();
        $podcast->titulo      = $request->titulo;
        $podcast->descripcion = $request->descripcion;

        $music_file = $request->file('archivo');
        $nombre     = $request->archivo->getClientOriginalName();
        $filename   = $nombre;
        $location   = public_path('audio/');
        $music_file->move($location,$filename);
        $podcast->archivo = $nombre;

        if($podcast->save()) {
            $response['status'] = true;
            $response["message"] = "El podcast se ha registrado correctamente.";
            $response['url'] = route('podcast_index');
        }


        return response()->json($response);

    }

    public function edit($id){
        $podcast = Podcast::find(base64_decode($id));

        if ($podcast != null) {
            return view('back.dashboard.system.podcast.edit', ['podcast' => $podcast]);
        }else{
            return redirect()->route('podcast_index');
        }
    }

    public function update(EditRequest $request, $id){

        $response = array('status'=>false);
        $podcast = Podcast::find(base64_decode($id));
        $podcast->titulo = $request->titulo;
        $podcast->descripcion = $request->descripcion;

        $music_file = $request->file('archivo');

        if( $music_file ){
            //si recibe un file elimina el anterior y carga el nuevo
            $nombre     = $request->archivo->getClientOriginalName();
            $filename   = $nombre;
            $location   = public_path('audio/');
            File::delete(File::glob($location.$podcast->archivo));
            $music_file->move($location,$filename);
            $podcast->archivo = $nombre;
        }

        if($podcast->save()) {
            $response['status'] = true;
            $response["message"] = "El podcast se ha registrado correctamente.";
            $response['url'] = route('podcast_index');
        }


        return response()->json($response);
    }

    public function destroy($id){
        $response = array('status'=>false);

        $podcast = Podcast::find(base64_decode($id));
        $response = array(
            'status' => true,
            'message' => 'El podcast ha sido eliminado correctamente.',
            'url' => route('podcast_index'),
        );
        if ($podcast != null) {
            if (!$podcast->delete()) {
                $response = [
                    'message' => 'El podcast no se encuentra dado de alta en el sistema.',
                    'status' => false
                ];
            }else{
                $location   = public_path('audio/');
                File::delete(File::glob($location.$podcast->archivo));
            }
        }

        return response()->json($response);
    }
}

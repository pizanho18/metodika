<?php

namespace MetodikaTI\Http\Requests\Back\System\Podcast;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateRequest extends FormRequest
{
    //protected $table = 'podcast';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::check() ? true : false);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|min:3',
            'descripcion' => 'required|min:3|max:50',
            'archivo' => 'required|mimes:mpga,wav',
        ];
    }

}

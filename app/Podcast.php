<?php

namespace MetodikaTI;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Podcast extends Model
{

    use Notifiable;

    protected $table = 'podcast';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo', 'descripcion', 'archivo',
    ];

}

@extends('back.layout.account_client')

{{-- Page Title --}}
@section('pageTitle')
    Restablecer contraseña
@stop

{{-- Main Content --}}
@section('content_account_client')


    <div class="login-register" style="background-image:url(assets/images/background/login-register.jpg);">
        <div class="login-box card">
            <div class="card-body">

                @if (!$error)

                    {{ Form::open(['route' => ['update_password', $token], 'class' => 'form-horizontal form-material', 'id' => 'change_password']) }}

                    {{ Form::hidden('type', "clients") }}

                    <h5 class="text-center m-b-20">Favor de introducir y confirmar contraseña:</h5>
                    <br>

                    <div class="form-group">
                        <div class="col-xs-12">
                            {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Nueva contraseña', 'required' => "true"]) }}
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            {{ Form::password('password_confirmed', ['class' => 'form-control', 'placeholder' => 'Confirmar nueva contraseña', 'required' => "true"]) }}
                        </div>
                    </div>


                    <div class="col-xs-12">
                        <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Cambiar contraseña</button>
                    </div>

                    {{ Form::close()  }}

                @else

                    <br>
                    <br>
                    <h5 class="text-center m-b-20">El link proporcionado a caducado o no existe, regresa a la pantalla principal y solicita nuevamente la recuperación de contraseña.</h5>
                    <div class="text-center">
                        <a class="text-center" href="{{ url("/") }}">Ir a pantalla principal</a>
                    </div>
                    <br>

                @endif

            </div>
        </div>
    </div>

@stop
 
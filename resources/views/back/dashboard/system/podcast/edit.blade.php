@extends('back.layout.dashboard')

@section('contentTitle', 'Edicion de podcast')

@section('topButton')
    <a href="{{ route('podcast_index') }}" class="m-l-15"><button type="button" class="btn btn-info d-none d-lg-block"><i class="fas fa-chevron-left"></i> Regresar</button></a>
@stop

@section('content_dashboard')

<div class="row">
    <div class="col-sm-12">

        <div class="card card-body">

            {!! Form::open(['route' => array('podcast.update', base64_encode($podcast->id) ), 'class' => 'form-horizontal']) !!}

                <div class="form-group">
                    <label>Titulo</label>
                    {{ Form::text('titulo', $podcast->titulo, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    <label>Descripción</label>
                    {{ Form::textArea('descripcion', $podcast->descripcion, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">

                    <audio src="../../../../public/audio/{{$podcast->archivo}}" controls>
                        {{$podcast->archivo}}
                    </audio><br>
                    {{$podcast->archivo}}
                </div>

                <div class="alert alert-warning">
                    Solo si se selecciona un nuevo archivo de audio se actualizara
                </div>

                <div class="form-group">
                    <label>Archivo</label>
                    {{ Form::file('archivo', null, ['class' => 'form-control']) }}
                </div>

                <button type="submit" class="btn btn-primary">Guardar</button>

            {!! Form::close() !!}

        </div>
    </div>
</div>

@stop

@section('JS')
    {{ Html::script('system/js/form.js')  }}
@stop
